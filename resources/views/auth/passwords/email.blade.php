@extends('layouts.master')

@section('content')

    <div class="login-container">
        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content">
                        @successMessage('status')

                    <!-- Password recovery -->
                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}
                            <div class="panel panel-body login-form">
                                <div class="text-center">
                                    <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
                                    <h5 class="content-group">Password recovery <small class="display-block">We'll send you instructions in email</small></h5>
                                </div>

                                <div class="form-group has-feedback @errorClass('email')">
                                    <input type="email" class="form-control" placeholder="Your email" name="email" value="{{ old('email') }}">
                                    <div class="form-control-feedback">
                                        <i class="icon-mail5 text-muted"></i>
                                    </div>
                                    @errorBlock('email')
                                </div>

                                <button type="submit" class="btn bg-blue btn-block">Reset Password<i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </form>
                        <!-- /password recovery -->


                        <!-- Footer -->
                        <div class="footer text-muted text-center">
                            Copyright &copy; {{  date('Y') }}
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
    </div>
@endsection
