@extends('layouts.master')
@section('content')
@include('layouts.navbar')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

		@include('layouts.side-navigation')

		<!-- Main content -->
			<div class="content-wrapper">
				<div class="container">
					<div class="content error-page-wrapper">
						<h2 class="title">Error: Bad Request</h2>
						<img src="/images/error400.png" alt="">
						<p>Your request resulted in an error. <a href="{{route('home')}}">Homepage</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
