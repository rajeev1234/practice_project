@extends('layouts.master')
@section('content')
    @include('layouts.navbar')

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

        @include('layouts.side-navigation')

        <!-- Main content -->
            <div class="content-wrapper">
                <div class="container">
                    <div class="content error-page-wrapper">
                        <h2 class="title">Error: 503 Forbidden</h2>
                        <img src="/images/error503.png" alt="">
                        <p>Service Unavailable. <a href="{{route('home')}}">Homepage</a></p>
                    </div>
                </div>
                <div class="container">
                    <div class="content">
                        <div class="title">Be right back.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
