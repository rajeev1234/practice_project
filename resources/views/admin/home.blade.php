@extends('layouts.admin')
@section('title')
    Dashboard
@endsection
@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>


        </div>
    </div>
    <!-- /page header -->
    <div class="content">

        @successMessage('success-message')

        Admin Dashboard
    </div>

@endsection


