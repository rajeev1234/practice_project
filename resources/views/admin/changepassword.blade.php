
@extends('layouts.admin')

@section('content')
	<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-default">
				<div class="page-header-content">
					<div class="page-title">
						<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Change Password</h4>
					</div>
				</div>

				<div class="breadcrumb-line">
					<ul class="breadcrumb">
						<li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
						<li class="active">Change Password</li>
					</ul>

				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">
				@successMessage('success-message')

				<!-- Change Password Form -->
				<div class="panel panel-default" style="margin-top: 2em;">
					<div class="panel-heading">Change Password</div>
					<div class="panel-body">

						{!! Form::open(['route' => 'updatepassword','id'=>'changepassword']) !!}

						<div class="form-group  form-group-material @errorClass('old_password')">
							{!! Form::label('old_password', 'Old Password',['class'=>"control-label  is-visible"]) !!}
							{!! Form::password('old_password',array('class'=>'form-control required ','data-trigger'=>'change focusout','data-required-message'=>'Please enter old password')) !!}
							@errorBlock('old_password')
						</div>
						<div class="form-group  form-group-material @errorClass('password')">
							{!! Form::label('password', 'New Password',['class'=>"control-label  is-visible"]) !!}
							{!! Form::password('password',array('class'=>'form-control required ','data-trigger'=>'change focusout','data-required-message'=>'Please enter password')) !!}
							@errorBlock('password')
						</div>
						<div class="form-group  form-group-material @errorClass('confirm_password')">
							{!! Form::label('confirm_password', 'Confirm Password',['class'=>"control-label  is-visible"]) !!}
							{!! Form::password('confirm_password',array('class'=>'form-control required','data-trigger'=>'change focusout','data-equalto'=>'#password','data-required-message'=>'Please re-enter password','data-equalto-message'=>'Password didn\'t match with confirm password')) !!}
							@errorBlock('confirm_password')

						</div>

						{!! Form::submit('Change Password',array('class'=>'btn btn-primary')) !!}
						{!! Form::close() !!}
					</div>
				</div>

				<!-- /Change Password Form -->

			</div>
			<!-- /Content area -->

		</div>
@endsection

