@extends('layouts.admin')
@section('title')
    {{$title}}
@endsection
@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - {{$title}}</h4>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">{{$title}}</li>
            </ul>


        </div>
    </div>
    <!-- /page header -->
    <div class="content">

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{$title}}</h5>
            </div>
            <div class="panel-body">
                @successMessage('success-message')

                <table class="table datatable-basic">
                    <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role}}</td>
                        <td>@if($user->status==1)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">Inactive</span>
                            @endif
                        </td>
                        <td class="text-center">
                            @if($user->id!=Auth::user()->id)
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        @if($user->status==1)
                                            <li><a href="{{ route('user.resetpassword', $user->hash) }}"><i class=" icon-key"></i>Reset Password</a></li>
                                            <li><a href="{{ route('user.changestatus', [$user->hash,0]) }}" onclick="return confirm('Are you sure?')"><i class="icon-user-block"></i>User Inactive</a></li>
                                        @elseif($user->status==0)
                                             <li><a href="{{ route('user.changestatus', [$user->hash,1]) }}" onclick="return confirm('Are you sure?')" ><i class="icon-user-check"></i>User Active</a></li>
                                        @endif
                                    </ul>
                                </li>
                            </ul>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection


