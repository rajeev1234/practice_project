
@extends('layouts.admin')

@section('content')
<div class="container-fluid">
	<div class="row">

		<div class="col-md-4 col-md-offset-2" style="position: fixed;top: 20%;left: 15%;width: 45em;">
			<div class="panel panel-default">
				<div class="panel-heading">Reset Password</div>
				<div class="panel-body">

					{!! Form::open(['url'=> route('user.newpassword',$hash),'id'=>'resetpassword','novalidate'=>'novalidate']) !!}

						<div class="form-group  form-group-material {{($errors->first('password'))?'has-error':''}}">
							{!! Form::label('password', 'New Password',['class'=>"control-label  is-visible"]) !!}
							{!! Form::password('password',array('class'=>'form-control required ','data-trigger'=>'change focusout','data-required-message'=>'Please enter password')) !!}
							@if($errors->first('password'))
								<span class="error {{($errors->first('password'))?'help-block':''}}">{{$errors->first('password')}}</span>
							@endif
						</div>
						<div class="form-group  form-group-material {{($errors->first('confirm_password'))?'has-error':''}}">
							{!! Form::label('confirm_password', 'Confirm Password',['class'=>"control-label  is-visible"]) !!}
							{!! Form::password('confirm_password',array('class'=>'form-control required','data-trigger'=>'change focusout','data-equalto'=>'#password','data-required-message'=>'Please re-enter password','data-equalto-message'=>'Password didn\'t match with confirm password')) !!}
							@if($errors->first('confirm_password'))
								<span class="error {{($errors->first('confirm_password'))?'help-block':''}}">{{$errors->first('confirm_password')}}</span>
							@endif
							
						</div>
                        
						{!! Form::submit('Reset',array('class'=>'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

