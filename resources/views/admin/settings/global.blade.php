@extends('layouts.admin')
@section('title')
    General
@endsection
@section('content')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i>
                    <span class="text-semibold"> Site Settings</span> - General
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Site Settings</li>

            </ul>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('success-message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span>
                </button>
                {{ Session::get('success-message')}}
            </div>
        @endif
        @if(Session::has('error-message'))
            <div class="alert alert-danger  alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span>
                </button>
                {!!Session::get('error-message')!!}
            </div>
        @endif
    </div>
    {!! Form::open(['route' => 'admin.settings.global']) !!}
    <div class="col-md-6">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h6 class="panel-title">General</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="text-bold">Title</label>
                    <input type="text" class="form-control" name="general[title]"
                           value="{{ array_get($globalSettings, 'value.general.title') }}">
                </div>
                <div class="form-group">
                    <label class="text-bold">Keywords</label>
                    <input type="text" class="form-control" name="general[keywords]"
                           value="{{ array_get($globalSettings, 'value.general.keywords') }}">
                </div>

                <div class="form-group form-group-material {{($errors->first('content'))?'has-error':''}}">
                    {!! Form::label('general[description]','Description',['class'=>"control-label is-visible text-bold"]) !!}
                    {!! Form::textarea('general[description]',array_get($globalSettings, 'value.general.description'),['id'=>'editor-full','class'=>'form-control']) !!}
                </div>


            </div>
        </div>

    </div>
    <div class=" col-md-6">
        <div class="panel panel-white panel-collapsed">
            <div class="panel-heading">
                <h6 class="panel-title">Advertise</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse" class=""></a></li>
                    </ul>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group form-group-material {{($errors->first('content'))?'has-error':''}}">
                    {!! Form::label('advertise','Advertise',['class'=>"control-label is-visible"]) !!}
                    {!! Form::textarea('advertise',array_get($globalSettings, 'value.advertise'),['id'=>'editor-full','class'=>'form-control summernote'])!!}
                </div>
            </div>
        </div>

        <div class="panel panel-white panel-collapsed">
            <div class="panel-heading">
                <h6 class="panel-title">Contacts</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse" class=""></a></li>
                    </ul>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="text-bold">Primary Email (for contact forms)</label>
                    <input type="email" class="form-control" name="contacts[primary_email]"
                           value="{{ array_get($globalSettings, 'value.contacts.primary_email') }}">
                </div>
                <div class="form-group">
                    <label class="text-bold">Display Email (for headers and footers)</label>
                    <input type="email" class="form-control" name="contacts[display_email]"
                           value="{{ array_get($globalSettings, 'value.contacts.display_email') }}">
                </div>
                <div class="form-group">
                    <label class="text-bold">Display Phone (for header and footers)</label>
                    <input type="text" class="form-control" name="contacts[display_phone]"
                           value="{{ array_get($globalSettings, 'value.contacts.display_phone') }}">
                </div>
                <div class="form-group">
                    <label class="text-bold">Additional Emails (to also recieve contacts - comma
                        separated)</label>
                    <input type="text" class="form-control" name="contacts[additional_emails]"
                           value="{{ array_get($globalSettings, 'value.contacts.additional_emails') }}">
                </div>
                <div class="form-group">
                    <label class="text-bold">Address</label>
                    <textarea name="contacts[address]" rows="4" style="resize:none;"
                              class="form-control">{{ array_get($globalSettings, 'value.contacts.address') }}</textarea>
                </div>
            </div>
        </div>

        <div class="panel panel-white panel-collapsed">
            <div class="panel-heading">
                <h6 class="panel-title">Social Media Links</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse" class=""></a></li>
                    </ul>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="text-bold">Facebook</label>
                    <table class="table table-borderless">
                        <tr>
                            <td>Profile URL:</td>
                            <td><input type="text" name="social_media[facebook]" class="form-control"
                                       value="{{ array_get($globalSettings, 'value.social_media.facebook') }}">

                            </td>
                        </tr>
                    </table>
                </div>

                <div class="form-group">
                    <label class="text-bold">Twitter</label>
                    <table class="table table-borderless">
                        <tr>
                            <td>Profile URL:</td>
                            <td><input type="text" name="social_media[twitter]" class="form-control"
                                       value="{{ array_get($globalSettings, 'value.social_media.twitter') }}">

                            </td>
                        </tr>
                    </table>
                </div>

                <div class="form-group">
                    <label class="text-bold">Linkedin URL</label>
                    <table class="table table-borderless">
                        <tr>
                            <td>Profile URL:</td>
                            <td>
                                <input type="text" name="social_media[linkedin]" class="form-control"
                                       value="{{ array_get($globalSettings, 'value.social_media.linkedin') }}">

                            </td>
                        </tr>
                    </table>
                </div>

                <div class="form-group">
                    <label class="text-bold">Instagram URL</label>
                    <table class="table table-borderless">
                        <tr>
                            <td>Profile URL:</td>
                            <td><input type="text" name="social_media[instagram]" class="form-control"
                                       value="{{ array_get($globalSettings, 'value.social_media.instagram') }}">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <label class="text-bold">Pinterest URL</label>
                    <table class="table table-borderless">
                        <tr>
                            <td>Profile URL:</td>
                            <td><input type="text" name="social_media[pinterest]" class="form-control"
                                       value="{{ array_get($globalSettings, 'value.social_media.pinterest') }}">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <label class="text-bold">Google Plus</label>
                    <table class="table table-borderless">
                        <tr>
                            <td>Profile URL:</td>
                            <td><input type="text" name="social_media[google_plus]" class="form-control"
                                       value="{{ array_get($globalSettings, 'value.social_media.google_plus') }}">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <input type="submit" class="btn btn-primary" value="SUBMIT">
        {!! Form::close() !!}
    </div>
@endsection

