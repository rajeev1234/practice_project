<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="{{asset('/images/avatar.jpg')}}" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold">{{ (Auth::user())?Auth::user()->name:null }}</span>
                    </div>

                </div>
            </div>
        </div>
        <!-- /user menu -->
        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="active"><a href="{{route('dashboard')}}"><i class="icon-home4"></i><span>Dashboard</span></a></li>
                    <li><a href="{{route('admin.users.index')}}"><i class="icon-users"></i><span>Users</span></a></li>
                    <li><a href="{{ route('admin.settings.global') }}"><i class="icon-cog3"></i><span>Site Setting</span></a></li>
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->

<script>

    var full_url = window.location.href;
    var url = window.location;
    var protocol = url.protocol;
    var hostname = url.hostname;
    var pathname = url.pathname;
    var path = pathname.split('/');
    url = protocol + '//' + hostname + '/' + path[1];


    var exact_route_not_found = true;

    $('ul li a').filter(function () {
        if (this.href == full_url) {
            $('ul li').removeClass('active');
            exact_route_not_found = false;
            return true;
        }
    }).parent().addClass('active');


    if (exact_route_not_found) {
        // Will also work for relative and absolute hrefs
        $('ul li a').filter(function () {
            $('ul li').removeClass('active');
            return this.href == url;
        }).parent().addClass('active');
    }

</script>