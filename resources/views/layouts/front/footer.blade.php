<!--Footer-->
<!-- Overlay for Property image upload processing -->
<!--<div id="overlay"></div>-->
<footer class="site-footer">
    <div class="wrapper">
        <div class="main-footer row">
            <div class="col-md-4 col-sm-4 locations">
                <h3>Top Location Worldwide</h3>
                <ul>
                @foreach($top_location_countries as $top_location)
                        <li><a href="{{'/'.$top_location->slug}}">{{ $top_location->name }}</a></li>
                @endforeach
                </ul>

            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4 col-sm-4 contact">
                <h3>Contact</h3>

                <p>
                    {{isset($global_value['contacts']['display_email'])?$global_value['contacts']['display_email']:''}}

                </p>
                @if(isset($global_value['social_media']))
                    @php
                        $social_links= $global_value['social_media'];
                        $social_icons=array('facebook'=>'facebook-square','twitter'=>'twitter-square','linkedin'=>'linkedin-square','instagram'=>'instagram');
                    @endphp
                    <ul class="socials">
                        @foreach($social_links as $key=>$links)
                            @if(isset($social_icons[$key]))
                                <li><a href="{{ $links }}" target="_blank"><i
                                                class="fa fa-{{$social_icons[$key]}}"></i></a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="col-md-3 col-sm-4 newsletter label-design">

                <h3>Newsletter</h3>
                <p>Good wholesome stuff, once a month or therebouts.
                </p>

                <form  method="POST" action="{{route('newsletter_subscription')}}" novalidate='novalidate'>
                    {{csrf_field()}}
                    <div class="label-design{{ (isset($errors) && $errors->has('name')) ? ' has-error' : '' }}">
                        <span>
                            <label for="news_name">Name</label>
                        <input type="text" name="name" id="news_name" value="{{ old('name') }}">
                        </span>
                        @if (isset($errors) && $errors->has('name'))
                            <span class="help-block">
                                 <strong>{{ $errors->first('name') }}</strong>
                             </span>
                        @endif
                    </div>
                    <div class="form-group{{ (isset($errors) && $errors->has('email')) ? ' has-error' : '' }}">
                        <span>
                            <label for="news_email">E-mail</label>
                            <input type="email" name="email" id="news_email" value="{{ old('email') }}">
                        </span>
                        @if (isset($errors) && $errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <input type="submit" value="Sign Me Up" class="submit-btn lined">
                </form>

            </div>
        </div>

        <div class="footer-menu">
            <ul>
                <li><a href="{{route('search')}}">Properties</a></li>
                {{--<li><a href="{{route('pages.currency')}}">Currency</a></li>--}}
                <li><a href="{{route('pages.contact')}}">Contact Us</a></li>
                <li><a href="{{route('help_center.faq')}}">FAQs</a></li>
                <li><a href="{{route('pages.career')}}">Careers</a></li>
                <li><a href="{{route('help_center.term_condition')}}">Terms & Conditions</a></li>
                <li><a href="{{route('help_center.privacy_policy')}}">Privacy Policy</a></li>
                {{--<li><a href="{{route('pages.data_service')}}">Data Service</a></li>--}}
                <li><a href="{{route('pages.advertise')}}">Advertise</a></li>
                <li><a href="{{route('pages.buyers_guides')}}">Buyer's Guides</a></li>
                <li><a href="{{route('pages.feedback')}}">Website Feedback</a></li>
            </ul>
        </div>
    </div>
    <div class="copyright">
        &copy; {{  date('Y') }} SnowOnly. All rights reserved
    </div>
</footer>
