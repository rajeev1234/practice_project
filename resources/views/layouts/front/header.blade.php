<header class="site-header" id="header" role="banner">

    <nav class="navigation navbar ">
        <div class="wrapper">
            <figure class="site-logo floatleft">
                <a href="{{route('home')}}"><img src="/images/logo.png" alt="SnowOnly"></a>
            </figure>
            <div id="navbar" class="floatright">
                <ul class="nav navbar-nav navbar-right">
                    <li class="menu-parent">
                        <a href="javascript:void(0)" class="parent-anchor ">Ski Resort & Weather</a>
                        <span class="responsiveOnly in-navigator"><i class="fa fa-angle-right"></i> </span>
                        <div class="drop-down sub-menu">
                            <span class="megamenu-closebtn">
                                <img src="/images/close-btn.png" alt="Close Btn">
                            </span>

                            <span class="responsiveOnly out-navigator"><i class="fa fa-angle-left"></i> <span>BACK</span></span>
                            <div class="wrapper">
                                <div class="row">
                                    <div class="col-md-3 country">
                                        <h3>Country</h3>
                                        <ul class="country_list scroll-y">
                                            @foreach($countries_locations as $country=>$locations)
                                            <li><a href="{{ $country }}" class="country_name">{{ $country }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-md-9 column-3 resort-wrap">
                                        <h3>Resort</h3>
                                        <ul class="resort scroll-y">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="{{route('pages.find_agent')}}">Find agent</a></li>
                    <li><a href="{{route('pages.about_us')}}">about</a></li>
                    <li><a href="{{route('pages.advertise')}}">advertise</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Info <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('blogs.list')}}">Articles</a></li>
                            <li><a href="{{route('pages.buyers_guides')}}">Buyer's Guides</a></li>

                        </ul>
                    </li>
                    <li><a href="{{route('pages.contact')}}">contact</a></li>
                    @if(\Auth::user())

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{url('/'.str_replace('_', '-',\Auth::user()->type))}}">Dashboard</a>
                                </li>
                                <li>
                                    <a href="#" onclick="document.getElementById('logoutForm').submit();">Log out</a>
                                    <form action="{{route('logout')}}" id="logoutForm" method="post">{{csrf_field()}}</form>
                                </li>
                                <li>
                                    <a href="mailto:support@snowonly.com?subject=Help">Help</a>
                                </li>
                            </ul>
                        </li>


                    @else
                        <li><a href="{{route('login').'#signin'}}">register / sign in</a></li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->
            <a class="nav-toggle toggle-design" aria-hidden="false">Menu
                <span class="icon">
                    <span class="line1"></span>
                    <span class="line2"></span>
                    <span class="line3"></span>
                </span>
            </a>
            <div class="clear"></div>
        </div>
    </nav>
</header>

@php

    $property_type = array_map(function($value)
    {
        return str_slug($value);
    }, \Config::get('property.property_type'));

    $property_have = array_map(function($value)
    {
        return str_slug($value);
    }, \Config::get('property.property_have.add_property'));

@endphp



<script>
    var country_resorts =<?php echo json_encode($countries_resorts); ?>;
    var country_resorts_and_slugs =<?php echo json_encode($countries_resorts_and_slugs); ?>;

    var country_locations =<?php echo json_encode($countries_locations); ?>;

    var property_search_route ='<?php echo route('property.search'); ?>';

    var property_type =<?php echo json_encode($property_type); ?>;

    var property_have =<?php echo json_encode($property_have); ?>;
</script>
