
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Practice Project-Admin</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>var activeRoute = '{{ \Route::currentRouteName() }}';</script>
    @routes
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/admin/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/admin/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/admin/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/admin/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/admin/css/colors.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('assets/admin/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/loaders/blockui.min.js')}}"></script>


    <!-- /core JS files -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/pages/datatables_basic.js')}}"></script>
    <!-- /core JS files -->


    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/editors/summernote/summernote.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <!-- Discount Percent 0 to 100 range -->
    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/forms/inputs/touchspin.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/uploaders/fileinput.min.js')}}"></script>
    <!-- /Discount Percent 0 to 100 range -->

    <script type="text/javascript" src="{{asset('assets/admin/js/core/app.js')}}"></script>
    <!-- /theme JS files -->

    <!-- Datepicker JS files -->
    <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
    <!-- /Datepicker JS files -->

    <script>

        $( document ).ready(function() {

            /*----summernote-----*/
            $('.summernote').summernote({
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link', 'picture']],
                    ['view',['fullscreen','codeview']],
                ]
            });
            /*----summernote-----*/

        });
    </script>


</head>
@if(!Request::is('admin/login'))
    <body>
    <!-- top navbar -->
    @include('layouts.admin.navbar')

    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
        <!-- sidebar -->
            @include('layouts.admin.side-navigation')
            @yield('content')
        </div>
        <!-- /page content -->
    </div>
    <!-- /page container -->
    </body>
@else
    <body class="login-container">
        @yield('content')
    </body>
@endif

</html>
