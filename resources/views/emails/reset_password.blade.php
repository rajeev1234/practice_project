<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Define Charset -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><!-- Responsive Meta Tag -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
    <title>SnowOnly</title><!-- Responsive Styles and Valid Styles -->

    <style type="text/css">
        /*<![CDATA[*/
        img{
            display: block; margin:0; padding:0; border:none;
        }
        body {
            width: 100%;
            background-color: #fff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }
        a{
            overflow-wrap: break-word;
            word-wrap: break-word;

            -ms-word-break: break-all;
            /* This is the dangerous one in WebKit, as it breaks things wherever */
            word-break: break-all;
            /* Instead use this non-standard one: */
            word-break: break-word;

            /* Adds a hyphen where the word breaks, if supported (No Blink) */
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }

        p, h1, h2, h3, h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 14px;
            border: 0;
        }

        /* ----------- responsivity ----------- */
        @media only screen and (max-width: 640px) {
            /*------ top header ------ */
            .main-header {
                line-height: 28px !important;
            }

            .main-subheader {
                line-height: 28px !important;
            }

            /*--------logo-----------*/
            .logo {
                width: 128px !important;
                margin-left: 10px !important
            }

            .slogan {
                font-size: 14px !important;
            }

            /*----- main image -------*/
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }

            .hide-for-iphone {
                display: none !important;
            }

            .sidebar {
                height: auto !important;
            }

            /*--------- divider ----------*/
            .divider img {
                width: 420px !important;
                height: 1px !important;
            }

            /*--------- banner ----------*/
            .banner img {
                width: 430px !important;
                height: auto !important;
            }

            /*-------- container --------*/
            .container590 {
                width: 440px !important;
            }

            .container580 {
                width: 420px !important;
            }

            .container440 {
                width: 440px !important;
                float: left;
            }

            .container440 .container420 {
                width: 420px !important
            }

            .section-item {
                width: 190px !important;
                background: #fff;
                margin: 0 auto;
                float: none !important
            }

            .section-item td {
                text-align: center !important;
            }

            .section-item img {
                width: 100% !important;
                height: auto !important
            }

            .container590.fullwidth img {
                width: 100%
            }

            /*-------- secions ----------*/
            .cta-header {
                line-height: 26px !important;
            }

            /*-- --------- socials ----------- --*/
            .social-container {
                width: 215px !important;
            }

            .response {
                display: block;
                width: 100%;
            }

            /*.container590-img img{ width: 100%;}*/
            span.foot {
                display: block;
                text-align: center !important;
                width: 100%;
            }

            span.foot span {
                float: none !important;
                display: inline
            }

            .twocolumn td {
                display: block;
                width: 100%
            }

            .central-style {
                text-align: center;
                display: block
            }

            .single {
                display: block;
                width: 100%;
                padding: 8px 0
            }

            .middle a {
                margin: 0 auto;
                float: none;
            }

            .mainimage img {
                width: 100%
            }
        }

        @media only screen and (max-width: 479px) {

            /*------ top header ------ */
            .main-header {
                line-height: 28px !important;
            }

            .main-subheader {
                line-height: 28px !important;
            }

            /*--------logo-----------*/
            .logo {
                width: 280px !important;
            }

            .date {
                width: 280px !important;
            }

            .date-inside {
                width: 150px !important;
            }

            .hideforiphone {
                display: none !important;
            }

            .slogan {
                font-size: 16px !important;
                line-height: 26px !important;
            }

            /*----- main image -------*/
            .main-image img {
                width: 280px !important;
                height: auto !important;
            }

            .hide-for-iphone {
                display: none !important;
            }

            .sidebar {
                height: auto !important;
            }

            /*--------- divider ----------*/
            .divider img {
                width: 260px !important;
                height: 1px !important;
            }

            /*--------- banner ----------*/
            .banner img {
                width: 270px !important;
                height: auto !important;
            }

            /*-------- container --------*/
            .container590 {
                width: 330px !important;
            }

            .container580 {
                width: 260px !important;
            }

            .container440, .section-item {
                width: 280px !important;
            }

            .container440 .container420 {
                width: 260px !important
            }

            /*-------- secions ----------*/
            .section-img img {
                width: 280px !important;
                height: auto !important;
            }

            .cta-header {
                line-height: 26px !important;
            }

            /*-- --------- socials ----------- --*/
            .social-container {
                width: 280px !important;
            }

            .response-hide {
                display: none
            }

            .middle a {
                margin: 0 auto;
                float: none;
                width: 100% !important
            }
        }

        /*]]>*/
    </style>
</head>

<body>
<table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <!-- =======Header part starts from here ======= -->
    <tr>

        <td>
            <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0"
                   class="container590 container590-img"
                   style="line-height: 22px; font-family:tahoma, sans-serif; font-weight: normal" width="600">
                <tbody>
                <tr>
                    <td height="20"></td>
                    <td height="20"></td>
                    <td height="20"></td>
                </tr>
                </tbody>
            </table>

        </td>

    </tr>
    <tr>
        <td>
            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0"
                   class="container590 mainimage" width="596" style="line-height: 0">
                <tbody>
                <tr>
                    <td>
                        <a href="{{route('home')}}"><img src="{{$message->embed(public_path('/images/newsletters/logo.png'))}}" alt="Logo"/></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr> <!--Logo section-->
    <tr>
        <td>
            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0"
                   class="container590 mainimage" width="598"
                   style="line-height: 20px; font-family:tahoma, sans-serif; color: #26345a; font-size: 14px; font-weight: 300">
                <tbody>
                <tr>
                    <td width="3" bgcolor="#26345a"></td>
                    <td width="25" bgcolor="#fff"></td>
                    <td width="542" style="word-break:break-all;"><br>
                        <p>Dear <strong style="font-weight: bold">{{$userName}}</strong>,</p> <br>
                        <p>We have received a forgotten password request for your account.</p><br>
                        <p style="word-break: normal">If you made this request, please <a href="{{route('password.reset',$token)}}" style="color: #26345a; font-weight: bold" target="_blank">click here</a> to enter a new password. If there is a problem with the link above, please copy and paste the following link into your web browser:</p><br>

                        <p><strong style="font-weight: bold; word-break:break-all;">LINK <a href="{{route('password.reset',$token)}}" style="color: #26345a" target="_blank">{{route('password.reset',$token)}}</a></strong></p>
                        <br>
                        <p style="word-break: normal">If you did not make this request yourself then you can safely ignore this email. Your account details will remain unchanged.</p>
                        <br>
                        <p style="word-break: normal">Once you have used the link to enter a new password, you will be signed in and can continue using <a href="{{route('home')}}" style="color: #26345a; font-weight: bold" target="_blank">Practice Project</a>.</p>
                        <br>
                        <p>Thank you.</p>
                        <br>
                        <p><strong style="font-weight: bold">PRACTICE PROJECT</strong></p><br>
                    </td>
                    <td width="25" bgcolor="#fff"></td>
                    <td width="3" bgcolor="#26345a"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr> <!--Successful msg dailog box-->
    <tr>
        <td>
            <table align="center" bgcolor="#26345a" border="0" cellpadding="0" cellspacing="0"
                   class="container590 mainimage" width="598"
                   style="line-height: 20px; font-family:tahoma, sans-serif; color: #fff; font-size: 15px; font-weight: bold; text-align: center; border-radius: 0 0 5px 5px">
                <tbody>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td><p>&copy; {{  date('Y') }} Test Project. All rights reserved.</p>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>

        <td>
            <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0"
                   class="container590 container590-img"
                   style="line-height: 22px; font-family:tahoma, sans-serif; font-weight: normal" width="600">
                <tbody>
                <tr>
                    <td height="20"></td>
                    <td height="20"></td>
                    <td height="20"></td>
                </tr>
                </tbody>
            </table>

        </td>

    </tr> <!--footer-->
    </tbody>
</table>
</body>
</html>

