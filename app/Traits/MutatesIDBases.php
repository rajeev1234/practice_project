<?php namespace App\Traits;


use Illuminate\Database\Eloquent\Model;

/**
 * Trait MutatesIDBases
 * @package App\Traits
 *
 * To use this trait, add $table->string('hash', 12)->unique() to the corresponding migration
 * or create a corresponding column in the schema. When applied to a model, this trait will
 * fire on the boot of the model, listen to the creating even and generate a unique hash for the model.
 */
trait MutatesIDBases {

	/**
	 * Converts a given integer to base62 encoded string
	 *
	 * @param $value
	 *
	 * @return string
	 */
	private static function to_base_62( $value ) {
		$base = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_';

		$b      = 62;
		$r      = $value % $b;
		$result = $base[ $r ];
		$q      = floor( $value / $b );

		while ( $q ) {
			$r      = $q % $b;
			$q      = floor( $q / $b );
			$result = $base[ $r ] . $result;
		}

		return $result;
	}


	/**
	 * When a model is first created, sets its hash value to a unique base 62 encoded string.
	 * This is to generate youtube style short urls that can work without the use of internal id PK
	 */
	public static function bootMutatesIDBases() {

		/** @noinspection PhpUndefinedMethodInspection */
		static::creating( function ( Model $model ) {
			$table = $model->getTable();
			$db    = app()->make( 'db' );
			while ( true ) {
				$random = random_int( 1, PHP_INT_MAX );
				$hash   = static::to_base_62( $random );
				$count  = $db->table( $table )->where( 'hash', $hash )->count();
				if ( $count == 0 ) { //unique hash
					$model->setAttribute( 'hash', $hash );
					break;
				}
				continue;
			}
		} );
	}


}