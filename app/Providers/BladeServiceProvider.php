<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //custom blade directive to generate the active classes for menu items
        Blade::directive('isActive', function ($name) {
            return '<?php if(Request::route()->getName() === ' . $name . ') echo "active"; ?>';
        });

        //custom blade directive to render the has-error class if input has error
        Blade::directive('errorClass', function ($input) {
            return '<?php if ($errors->has(' . $input . ')) echo "has-error"; ?>';
        });

        //custom blade directive to render the error block if input has error
        Blade::directive('errorBlock', function ($input) {
            return
                '<?php if($errors->has(' . $input . ')):?>
                    <span class=\'error help-block\'>
                         <?php echo $errors->first(' . $input . ') ?>
                    </span>
                 <?php endif;?>';
        });

        //success session message flash
        Blade::directive('successMessage', function ($input) {
            return
                '<?php if (session()->has(' . $input . ')):?> 
                    <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span>
                        </button><?php echo session()->get(' . $input . ')?>
                    </div>
                <?php endif;?>';
        });

        //error session message flash
        Blade::directive('errorMessage', function ($input) {
            return
                '<?php if (session()->has(' . $input . ')):?> 
                    <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span>
                        </button><?php echo session()->get(' . $input . ')?>
                    </div>
                <?php endif;?>';
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
