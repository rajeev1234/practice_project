<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPasswordMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $token;
    protected $userModel;

    public function __construct($token, User $user)
    {
        $this->token = $token;
        $this->userModel = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $userEmail = $this->userModel->email;
        $userName = $this->userModel->name;
        $subject = 'Password Reset';

        return $this->view('emails.reset_password')
            ->to($userEmail)
            ->subject($subject)
            ->with([
                'token'   => $this->token,
                'userEmail' => $userEmail,
                'userName'  => $userName
            ]);
    }
}
