<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Setting extends Model
{
    protected $fillable = ['group', 'key', 'value'];

    public static function globalData()
    {
        return self::where('group', 'global')->get(['value'])->first();
    }

}
