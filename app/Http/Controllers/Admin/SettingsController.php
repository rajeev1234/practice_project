<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BuyerGuideHomeFormRequest;
use App\Http\Requests\SliderFormRequest;
use App\Property;
use App\Setting;
use App\Blog;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;

class SettingsController extends Controller
{

    public function getGlobalSettings()
    {
        $globalSettings = Setting::globalData();
        if ($globalSettings) {
            $globalSettings['value'] = json_decode($globalSettings['value'], true);
        }

        return view('admin.settings.global', [
                                                'pageTitle' => 'General Site Settings',
                                                'pageSubTitle' => '',
                                                'globalSettings' => $globalSettings
                                            ]);
    }

    public function setGlobalSettings(Request $request)
    {

        $input = $request->all();

        $globalSettings = json_decode(Setting::global() ['value'], true);

        $globalSettings['general']['title'] = $input['general']['title'] ??($globalSettings['general']['title']??null);
        $globalSettings['general']['keywords'] = $input['general']['keywords'] ??($globalSettings['general']['keywords']??null);
        $globalSettings['general']['description'] = $input['general']['description'] ??($globalSettings['general']['description']??null);

        $globalSettings['default_currency'] = $input['default_currency'] ?? ($globalSettings['default_currency']??null);

        $globalSettings['top_location']['location'] = $input['top_location'] ?? ($globalSettings['top_location']['location']?? null);
        $globalSettings['advertise'] = $input['advertise'] ?? ($globalSettings['advertise']?? null);

        $globalSettings['contacts']['primary_email'] = $input['contacts']['primary_email'] ?? ($globalSettings['contacts']['primary_email']??null);
        $globalSettings['contacts']['display_email'] = $input['contacts']['display_email'] ?? ($globalSettings['contacts']['display_email']??null);
        $globalSettings['contacts']['display_phone'] = $input['contacts']['display_phone'] ?? ($globalSettings['contacts']['display_phone']??null);
        $globalSettings['contacts']['additional_emails'] = $input['contacts']['additional_emails'] ?? ($globalSettings['contacts']['additional_emails']??null);
        $globalSettings['contacts']['address'] = $input['contacts']['address'] ?? ($globalSettings['contacts']['address']??null);

        $globalSettings['social_media']['facebook'] = $input['social_media']['facebook'] ??  ($globalSettings['social_media']['facebook']??null);
        $globalSettings['social_media']['twitter'] = $input['social_media']['twitter'] ??  ($globalSettings['social_media']['twitter']??null);
        $globalSettings['social_media']['linkedin'] = $input['social_media']['linkedin'] ??  ($globalSettings['social_media']['linkedin']??null);
        $globalSettings['social_media']['instagram'] = $input['social_media']['instagram'] ??  ($globalSettings['social_media']['instagram']??null);
        $globalSettings['social_media']['pinterest'] = $input['social_media']['pinterest'] ??  ($globalSettings['social_media']['pinterest']??null);
        $globalSettings['social_media']['google_plus'] = $input['social_media']['google_plus'] ??  ($globalSettings['social_media']['google_plus']??null);

        $settings = Setting::where('group', 'global');

        $setting_exist = $settings->get();
        if ($setting_exist->isEmpty()) {
            $setting = Setting::create([
                'group' => 'global',
                'key' => 'global',
                'value' => json_encode($globalSettings),
                'status' => '1']);
        } else {
            $setting = $settings->update(['value' => json_encode($globalSettings)]);
        }
        if ($setting) {
            return redirect(route('admin.settings.global'))->with('success-message', "Settings Updated");
        } else {
            return redirect()->back()->with('error-message', "Unable to update Settings.");
        }
    }

}