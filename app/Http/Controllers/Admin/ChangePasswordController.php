<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ChangePasswordFormRequest;

class ChangePasswordController extends Controller
{
    public function index()
    {
        return View('admin.changepassword');

    }

    public function save(ChangePasswordFormRequest $request)
    {
        $user=Auth::user();

        if (!Hash::check($request->input('old_password'), $user->password)) {

            return redirect()->back()->withErrors([
                'old_password' => 'Old password didn\'t matched with user password.'
            ]);
        } else {

            $user->password = Hash::make($request->input('password'));
            $result = $user->save();


            if ($result) {
                return redirect()->route('dashboard')->with('success-message', 'Password Changed Successfully.');
            }
        }
    }
}
