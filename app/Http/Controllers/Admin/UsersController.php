<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\ChangePasswordFormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index(){

        $title = 'Users';
        $users = User::all();

        return view('admin.user.index',compact('title','users'));

    }


    public function changestatus($hash,$status)
    {
        $user=User::where('hash',$hash)->first();

        if(!$user) {
            abort(403);

        }else{
            $user->update(['status' => $status]);

            if($status=='0') {
                $message= "User is Inactive now.";
            }
            else if($status=='1') {
                $message="User is Active now.";
            }

            return redirect()->back()->with('success-message',$message);
        }

    }

    public function resetpassword($hash)
    {
        return View('admin.user.resetpassword',compact('hash'));
    }

    public function newpassword($hash,Request $request)
    {
        $user=User::where('hash',$hash)->first();

        if(!$user){
            abort(403);
        }else {

            $request->validate([
                'password' => 'required|required_with:confirm_password|same:confirm_password',
                'confirm_password'=>'required|required_with:password'
            ]);

            $user->password = Hash::make($request->input('password'));
            $user->status = 1;
            $user->save();
            
            return redirect()->route('admin.users.index')->with('success-message', 'User password has been changed.');
        }

    }

}
