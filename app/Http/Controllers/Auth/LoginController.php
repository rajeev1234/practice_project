<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function authenticate(Request $request){

        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);


        $email = $request->input('email');
        $password = $request->input('password');
        if (Auth::attempt(['email' => $email, 'password' => $password,'type'=>'admin'])) {
            // Authentication passed...
            return redirect()->intended('/admin');
        } else {
            return redirect()->back()->with('error-message', 'Username or password didn\'t matched.');
        }

    }


    public function showLoginForm()
    {
        $countries = \Countries::all()->pluck('name.common','name.common')->toArray();
        $currencies = \Config::get('property.currencies');
        return view('auth.register',compact('countries','currencies'));
    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $email = $request->input('email');
        $password = $request->input('password');



        $user_exist=User::where('email',$email)->first();

        if($user_exist) {
            $check = app('hash')->check($password, $user_exist->password);
            if($check) {
                if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1])) {

                    // Authentication passed...
                    $user = Auth::user();
                    if (\Config::get('project.role')[$user->role] !='Admin') {
                        return redirect()->intended($this->redirectTo);
                    } else {
                        return redirect()->intended('/admin');
                    }

                } else {
                    return redirect()->back()->with('error-message', 'Account not activated.');
                }

            }else{
                return redirect()->back()->with('error-message', 'Password didn\'t matched.');
            }
        }
        else{
            return redirect()->back()->with('error-message', 'Account with this email didn\'t exist.');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect($this->redirectTo);
    }
}
