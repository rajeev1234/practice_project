<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'password' => 'required|required_with:confirm_password|same:confirm_password',
            'confirm_password'=>'required|required_with:password',
        ];
    }
    public function messages()
    {
        return [
            'old_password.required' => 'Old password field is required',
            'password.required' => 'Password field is required',
            'confirm_password.required' => 'Confirm password field is required',

        ];
    }
}
