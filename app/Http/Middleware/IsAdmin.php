<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ! Auth::check() || \Config::get('project.role')[Auth::user()->role] !== "Admin") {
            Auth::logout();

            return redirect()->route('admin.login');
        }

        return $next($request);
    }
}
