<?php

rview('login', 'admin.login')->name('admin.login');
post('login', 'Auth\LoginController@authenticate')->name('authenticate');
get('logout', 'Auth\LoginController@logout')->name('admin.logout');


group(['middleware' => ['admin']], function () {


    get('/', 'AdminController@dashboard')->name('dashboard');
    group(['namespace' => 'Admin'], function () {

        get('users', 'UsersController@index')->name('admin.users.index');

        get('users/resetpassword/{hash}',
            ['as' => 'user.resetpassword', 'uses' => 'UsersController@resetpassword']);

        post('users/newpassword/{hash}', ['as' => 'user.newpassword', 'uses' => 'UsersController@newpassword']);

        get('users/changestatus/{hash}/{status}',
            ['as' => 'user.changestatus', 'uses' => 'UsersController@changestatus']);

        get('changepassword', ['as' => 'changepassword', 'uses' => 'ChangePasswordController@index']);
        post('updatepassword', ['as' => 'updatepassword', 'uses' => 'ChangePasswordController@save']);

        group(['prefix' => 'settings'], function () {

            /*--------------------------------------------------------------------------*/
            /* Global Setting Handler                                                   */
            /*--------------------------------------------------------------------------*/

           get('global', 'SettingsController@getGlobalSettings')->name("admin.settings.global");
           post('global', 'SettingsController@setGlobalSettings')->name("admin.settings.global");


        });


    });


});