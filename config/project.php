<?php

return [

    'status'=>[
        '0'=>'Inactive',
        '1'=>'Active'
    ],
    'role'=>[
        1=>'Admin',
        2=>'User'
    ],
];
