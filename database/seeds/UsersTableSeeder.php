<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $users = [
            [
                'name'          =>'Admin',
                'email'         =>'admin@gmail.com',
                'password'      =>Hash::make('admin'),
                'role'          =>1,
                'status'        =>1,
            ],
            [
                'name'          =>'Rajeev',
                'email'         =>'rajeevthapaliya@gmail.com',
                'password'      =>Hash::make('rajeev'),
                'role'          =>1,
                'status'        =>1,
            ]
        ];

        foreach ($users as $user) {
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            User::create($user);
        }
    }
}
