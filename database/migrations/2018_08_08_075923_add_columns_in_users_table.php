<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string( 'hash', 12 )->after('id')->unique();
            $table->string('phone')->after('password')->nullable();
            $table->integer('role')->after('phone')->nullable();
            $table->string('status')->after('role');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('hash');
            $table->dropColumn('phone');
            $table->dropColumn('role');
            $table->dropColumn('status');
            $table->dropSoftDeletes();
        });
    }
}
